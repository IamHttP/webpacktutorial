let conf = require('./webpack.config');

module.exports = Object.assign(conf, {
  devServer: {
    port: 9999
  },
  mode: 'development',
  devtool: 'inline-source-map'
});